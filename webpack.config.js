const path = require('path')

module.exports = {
    entry: path.resolve(__dirname, "src/index.js"),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: path.resolve(__dirname, "dist"),
        host: '192.168.1.4',
        historyApiFallback: true
    },
    resolve: {
        extensions: [".js", ".jsx"],
    },  
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: ["babel-loader"],
                exclude: /node_modules/,
            }
        ]
    },
    
}