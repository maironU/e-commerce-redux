import React from 'react'
import { ContainerBody, Title } from './styled'
import ProductList from '../ProductList'
import ShoppingCart from '../ShoppingCart'

const Body = () => {
    return(
        <>
            <Title>!Disfruta de los mejores productos!</Title>
            <ContainerBody>
                <ProductList />
            </ContainerBody>
        </>
    )
}

export default Body