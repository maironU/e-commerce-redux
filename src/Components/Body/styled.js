import styled from 'styled-components'

export const ContainerBody = styled.div`
    width: 95%;
    margin: 0 auto;
    display: flex;
`
export const Title = styled.h1`
    text-align: center;
`