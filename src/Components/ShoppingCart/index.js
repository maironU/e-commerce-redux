import React, { useEffect, useState } from 'react'
import { ContainerShoppingCart, ContainerNamePrice, Title, Ul, Total, Li,
     Name, Price, ContainerDetails, Image, ContainerPedido, Container, Span, ProcesarCompra, Cantidad,
     ContainerDeleteProduct, ShoppingCartEmpty, Link} from './styled'

import { MdDeleteForever } from 'react-icons/Md'
import { FaShoppingCart } from 'react-icons/fa'
import { useSelector, useDispatch } from 'react-redux'
import DeleteProductToShopping from '../../redux/actions/DeleteProductToShopping'

const ShoppingCart = () => {

    const [cart, setCart] = useState([])
    const dispatch = useDispatch()
    const remover = useSelector((state) => state.ShopingCartReducer)

    useEffect(() => {
        const MyCart = localStorage.getItem("cart")
        const MyCartParse = JSON.parse(MyCart)
        console.log(MyCartParse)

        setCart(MyCartParse)
    }, [remover])

    return(
        <>
            {cart != null ? 
                <>       
                    <Title>Carrito de Compras</Title>
                    <ContainerShoppingCart>
                        <Ul>
                            {cart.map(product => {
                                return(
                                    <Li key = {product.product.motel_id}>
                                        <ContainerDetails>
                                            <Image src={`http://192.168.1.4:8000/api/almacenamiento/${product.product.urlImage}`} />
                                            <ContainerNamePrice>
                                                <Name>{product.product.name}</Name>
                                                <Cantidad>Cantidad: {product.cantidad}</Cantidad>
                                                <Price>{product.product.price}</Price>
                                            </ContainerNamePrice>
                                        </ContainerDetails>
                                        <ContainerDeleteProduct onClick = { () => dispatch(DeleteProductToShopping(product))}>
                                            <MdDeleteForever size = {45} fill = "rgb(255, 0, 0)"/>
                                        </ContainerDeleteProduct>
                                    </Li>
                                    )
                                })

                            }
                        
                        </Ul>

                        <Title>Resumen de tu pedido</Title> 
                        <ContainerPedido>
                             <Container>
                                <Name>Cantidad</Name>
                                <Span>{cart.reduce((sum, product) => sum + parseInt(product.cantidad), 0)}</Span>
                            </Container>

                            <Container>
                                <Name>Total</Name>
                                <Span>${cart.reduce((sum, product) => sum + parseInt(product.product.price*product.cantidad), 0)}.000</Span>
                            </Container>
                        </ContainerPedido>
                </ContainerShoppingCart>

                
                <ProcesarCompra>
                    Procesar Compra
                </ProcesarCompra>
                </>
                :
                <ShoppingCartEmpty>
                    <FaShoppingCart size = {70} fill = "rgb(280, 0, 0)"/>
                    <h2>No hay Productos en el carrito de compras </h2>
                    <Link to = "/shop">Comprar Ahora</Link>
                </ShoppingCartEmpty>
            }       
        </>
    )
}

export default ShoppingCart