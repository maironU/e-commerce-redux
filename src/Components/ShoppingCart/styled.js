import styled from 'styled-components'
import {Link as LinkRouter} from 'react-router-dom'

export const ContainerShoppingCart = styled.div`
    width: 90%;
    margin: 0 auto;
    margin-bottom: 40px;
    display: flex;
    flex-flow: row wrap;
`
export const ContainerTitle = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #E3E4E8;
    padding: 5px;
    box-sizing: border-box;
`
export const Title = styled.h1`
    width: 100%;
    text-align: center;
`
export const Ul = styled.ul`
    width: 100%;
    margin: 0;

    @media (max-width: 768px) {
        padding: 0;
    }
`
export const Total = styled.span`
`
export const Li = styled.li`
    width: 100%;
    list-style: none;
    font-size: 18px;
    display: flex;
    padding: 20px 0 20px 0;
    border-bottom: 1px solid #eee;
    box-sizing: border-box;
    justify-content: space-between;
`
export const ContainerDetails = styled.div`
    display: flex;
`
export const Image = styled.img`
    width: 200px;
    height: 150px;

    @media (max-width: 768px) {
        width: 100px;
        height: 100px;
    }
`
export const ContainerNamePrice = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    margin-left: 20px;
`

export const ContainerPrice = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`
export const Price = styled.span`
    margin-right: 15px;
    color: red;
`
export const ContainerDeleteProduct = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
`
export const Cantidad = styled.span`

`
export const ContainerPedido = styled.div`
    width: 60%;
    margin: 0 auto;
    display: flex;
    flex-direction:column;
    border: 1px solid #eee;
    margin-bottom: 20px;

    @media (max-width: 768px) {
        width: 100%;
    }
`
export const Container = styled.div`
    width: 90%;
    margin: 0 auto;
    padding: 20px 0 20px 0;
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid #eee;

    :last-child {
        border: none;
    }
`
export const Name = styled.span`
    font-size: 20px;
`
export const Span = styled.span`
    color: red;
`
export const ProcesarCompra = styled.div`
    width: 55%;
    margin: 0 auto;
    padding: 10px;
    background: #f50;
    color: white;
    text-align: center;
    border-radius: 5px;
    box-sizing: border-box;
    margin-top: 20px;
    font-weight: 600;
    cursor: pointer;
    margin-bottom: 20px;

    @media (max-width: 768px) {
        width: 100%;
        position: fixed;
        bottom: 0;
        margin: 0;
    }
`
export const ShoppingCartEmpty = styled.div`
    width: 90%;
    margin: 30px auto;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    
    @media (max-width: 768px) {
        font-size: 10px;
    }
    
`
export const Link = styled(LinkRouter)`
    width: 30%;
    text-decoration: none;
    background: #f50;
    padding: 10px 0 10px 0;
    text-align: center;
    border-radius: 10px;
    color: white;
`
