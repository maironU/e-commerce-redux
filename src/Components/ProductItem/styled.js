import styled from 'styled-components'
import {Link as LinkRouter} from 'react-router-dom';


export const ContainerProductItem = styled.li`
    width: 22%;
    min-height: 340px;
    list-style: none;
    border: 1px solid #D5D5D5;
    margin: 10px 1% 10px 1%;
    margin-bottom: 15px;
    box-sizing: border-box;

    @media (max-width: 768px) {
       width: 100%;
	}
`
export const Link = styled(LinkRouter)`
    text-decoration: none;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
`
export const Product = styled.img`
    width: 100%;
    min-height: 75%;
`
export const ContainerNamePrice = styled.div`
    min-height: 25%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
    font-size: 20px;
    font-weight: 500;
`
export const NameProduct = styled.span`
    color: black;
`
export const Price = styled.span`
    padding: 5px;
    cursor: pointer;
    color: red;
`