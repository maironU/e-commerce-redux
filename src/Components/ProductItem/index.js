import React from 'react'
import { ContainerProductItem, Link, Product, ContainerNamePrice, NameProduct, Price } from './styled'
import { useDispatch } from 'react-redux'
import AddProductToShopping from '../../redux/actions/AddProductToShopping'

const ProductItem = (props) => {

    const dispatch = useDispatch()
    return(
        <ContainerProductItem>
        <Link to = {`/product/${props.motel_id}`}>
            <Product src={`http://192.168.1.4:8000/api/almacenamiento/${props.urlImage}`}/>
            <ContainerNamePrice>
                <NameProduct>{props.name}</NameProduct>
                <Price>
                    ${props.price}
                </Price>
            </ContainerNamePrice>
        </Link>
        </ContainerProductItem>
    )
}

export default ProductItem