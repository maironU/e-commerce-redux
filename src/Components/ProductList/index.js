import React, { useEffect } from 'react'
import { ContainerProductList } from './styled'
import { useSelector, useDispatch } from 'react-redux'
import FetchProducts from '../../redux/actions/ProductsAction'
import ProductItem from '../ProductItem'

const ProductList = () => {

    const dispatch = useDispatch()
    const products = useSelector((state) => state.ProductsReducer)

    console.log(products)

    useEffect(() => {
        dispatch((FetchProducts()))
    }, [])

    return(
        <>
            {products.products.length > 0 &&
                <ContainerProductList>
                    { products.products[0].map( (elem, key ) => <ProductItem key={key} {...elem}/> ) }
                </ContainerProductList>
            }
        </>
    )
}

export default ProductList