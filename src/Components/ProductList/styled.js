import styled from 'styled-components'

export const ContainerProductList = styled.ul`
    width: 100%;
    margin: 0;
    display: flex;
    flex-flow: row wrap;

    @media (max-width: 768px) {
        padding: 0;
     }
`