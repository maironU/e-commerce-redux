import React from 'react'
import { ContainerPreload, Preload, ContainerModal, Modal } from './styled'
import { useSelector } from 'react-redux'

const Preloader = () => {   

    const isLoadingProducts = useSelector((state) => state.ProductsReducer)
    const isLoadingProductWithId = useSelector((state) => state.ProductWithIdReducer)

    return(
        <>      
        {(isLoadingProducts.loading || isLoadingProductWithId.loading) &&
            <ContainerPreload>
                <Preload>
                </Preload>
            </ContainerPreload>
        }
        </>
    )
} 

export default Preloader