import React, { useEffect, useState } from 'react'
import { ContainerProductImage, Image, Title, ContainerDetails, ContainerSend, ContainerDetailsAndAddToCart,   
    ContainerLogoTitle, TitleSend, Price, ContainerAddToCart, Select,
     Option, AddToCart } from './styled'
import { useSelector, useDispatch } from 'react-redux'
import FetchProductWithId from '../../redux/actions/ProductWithIdAction'
import { FaCarSide } from 'react-icons/fa'
import AddProductToShopping from '../../redux/actions/AddProductToShopping'
import ModalAdd from '../ModalAdd'


const Details = ({product_id}) => {

    const dispatch = useDispatch()
    const product = useSelector((state) => state.ProductWithIdReducer)

    const [cantidad, setCantidad] = useState(1)
    const [seAnadio, setSeAnadio] = useState(false)

    useEffect(() => {
        dispatch(FetchProductWithId(product_id))
    },[])

    const CantidadSelected = () => {
        var selected = document.getElementById("cantidad");
        var cant = selected.options[selected.selectedIndex].text;
        setCantidad(cant)
    }

    const Anadir = (product) => {
        setSeAnadio(true)
        dispatch(AddProductToShopping(product, cantidad))
    }

    return(
        <ContainerDetails>
            <Title>Detalles del producto</Title>
                {product.product.length > 0 &&
                    <>
                        {product.product[0].map(product => {
                            return(
                                <ContainerProductImage key={product.motel_id}>
                                    <Image key = {product.motel_id} src= {`http://192.168.1.4:8000/api/almacenamiento/${product.urlImage}`}/>
                                    <ContainerDetailsAndAddToCart>
                                        <h2>{product.name}</h2>
                                        <Price>${product.price}</Price>
                                        <ContainerSend>
                                            <ContainerLogoTitle>
                                                <FaCarSide fill = "rgb(69, 160, 10)"/>
                                                <TitleSend>Envio Gratis</TitleSend>
                                            </ContainerLogoTitle>
                                            <span>Recíbelo el 27 de abril en Bogota</span>
                                        </ContainerSend>

                                        <ContainerAddToCart>
                                            <Select defaultValue={'DEFAULT'} id="cantidad" onChange = {() => CantidadSelected()}>
                                                <Option id="option-1" value="DEFAULT">1</Option>
                                                <Option id="option-2" value="2">2</Option>
                                                <Option id="option-3" value="3">3</Option>
                                                <Option id="option-4" value="4">4</Option>
                                                <Option id="option-5" value="5">5</Option>
                                                <Option id="option-6" value="6">6</Option>
                                            </Select>

                                            <AddToCart onClick = { () => Anadir(product)}>
                                                Añadir al carrito
                                            </AddToCart>
                                        </ContainerAddToCart>

                                    </ContainerDetailsAndAddToCart>
                                </ContainerProductImage>
                            )
                        })}
                    </>
                }
                {seAnadio &&
                    <ModalAdd />
                }
        </ContainerDetails>
    )
}

export default Details