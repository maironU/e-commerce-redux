import styled from 'styled-components'

export const ContainerDetails = styled.div`
    margin-bottom: 40px;
`

export const Title = styled.h3`
    text-align: center;
`
export const ContainerProductImage = styled.div`
    width: 95%;
    margin: 0 auto;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-around;
    margin-top: 20px;

`
export const Image = styled.img`
    width: 40%;
    height: 300px;
    @media (max-width: 768px) {
        width: 100%;
    }
`
export const ContainerDetailsAndAddToCart = styled.div`
    width: 45%;

    @media (max-width: 768px) {
        width: 100%;
        text-align: center;
     }
`
export const Price = styled.span`
    padding: 5px;
    font-weight: 600;
    color: red;
`
export const ContainerSend = styled.div`
    width: 100%;
    border: 1px solid #eee;
    display: flex;
    flex-direction: column;
    padding: 10px;
    border-radius: 5px;
    margin-top: 10px;
    box-sizing: border-box;

    @media (max-width: 768px) {
        width: 80%;
        margin: 0 auto;
     }
`
export const ContainerLogoTitle = styled.div`
    padding: 10px;
    color: #45a00a;
    font-weight: 600;
    @media (max-width: 768px) {
        padding: 0;
    }
`
export const TitleSend = styled.span`
    margin-left: 15px;
`
export const ContainerAddToCart = styled.div`
    margin-top: 10px;
    width: 100%;
    display: flex;
    justify-content: space-between;

    @media (max-width: 768px) {
        width: 80%;
        margin: 20px auto;
     }
`
export const Select = styled.select`
    width: 35%;
    border: 1px solid #eee;
    padding: 15px;
    margin- right: 5%;
`
export const Option = styled.option`
    font-size: 20px;
`
export const AddToCart = styled.div`
    width: 60%;
    border-radius: 5px;
    background: #f50;
    color: white;
    text-align: center;
    padding: 15px;
    box-sizing: border-box;
    cursor: pointer;
    font-size: 20px;
    font-weight: 600;

    @media (max-width: 768px) {
        font-size: 14px;
    }
`