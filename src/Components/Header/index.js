import React, {useEffect, useState} from 'react'
import { Container, Background, ContainerHeader, Ul, Li, 
    Link, LinkCart, ContainerLogo, LinkLogo, Logo, ContainerLogoMenu, ContainerInput, Input, ContainerTitle, Title, Span, Button,
    MyShoppingCart, Cant } from './styled'
import { FaCartPlus, FaShoppingCart } from 'react-icons/fa'
import { GiHamburgerMenu } from 'react-icons/gi'
import { useSelector } from 'react-redux'

const Header = () => {

    const [cart, setCart ] = useState([])
    const [none, setNone ] = useState(true)

    const cantidad = useSelector((state) => state.ShopingCartReducer)

    useEffect(() => {

        const cart = localStorage.getItem("cart")
        const cartParse = JSON.parse(cart)
        setCart(cartParse)
    }, [cantidad])


    let ul = document.getElementById("ul")
    let input = document.getElementById("input")
    let header = document.getElementById("header")

    const showUl = () => {
        if(screen.width <= 768 && none===true){
            ul.style.display = "none"
            input.style.display = "none"
            setNone(false)
        }

        if(ul.style.display === "none" && input.style.display === "none"){
            ul.style.display = "block"
            input.style.display = "block"
        }else{
            ul.style.display = "none"
            input.style.display = "none"
        }
    }

    window.onscroll = function() {
        if(window.scrollY >= 50 && screen.width > 768 ){
            header.style.backgroundColor = "rgba(0, 0, 0, 1)"
        }
        if(window.scrollY <= 50 && screen.width > 768){
            header.style.backgroundColor = "rgba(0, 0, 0, 0)"
        }

    }

    window.onresize = function(){
        if(window.innerWidth > 768 && window.scrollY <= 50){
            ul.style.display = "flex"
            input.style.display = "block"
            header.style.backgroundColor = "rgba(0, 0, 0, 0)"
        }else if(window.innerWidth > 768 && window.scrollY > 50){
            ul.style.display = "flex"
            input.style.display = "block"
            header.style.backgroundColor = "rgba(0, 0, 0, 1)"
        }else {
            ul.style.display = "none"
            input.style.display = "none"
            header.style.backgroundColor = "rgba(0, 0, 0, 1)"
        }
    }

    return(
        <Container>
            <Background src="https://cdn.pixabay.com/photo/2013/03/02/02/41/city-89197_960_720.jpg"/>

            <ContainerHeader id = "header">
                <ContainerLogo>
                    <LinkLogo to = "/shop">
                        <Logo src = "https://www.clubecommerce.com/wp-content/uploads/2017/11/club-ecommerce-2-1.png"/>
                    </LinkLogo>
                    <ContainerLogoMenu onClick = { () => showUl()}>
                        <GiHamburgerMenu size ={40} fill = "rgb(255, 255, 255)"/>
                    </ContainerLogoMenu>
                </ContainerLogo>

                <Ul id = "ul">
                    <Li>
                        <Link to = "">Inicio</Link>
                    </Li>
                    <Li>
                        <Link to = "">Categorias</Link>
                    </Li>
                    <Li>
                        <Link to = "">Acerca de nosotros</Link>
                    </Li>
                </Ul>

                <LinkCart to = "/cart">
                    <MyShoppingCart>
                        <FaShoppingCart size={30} fill = "rgb(255, 255, 255)"/>
                        {cart != null &&
                            <>
                                <Cant>{cart.reduce((suma, product) => suma + parseInt(product.cantidad), 0)}</Cant>
                            </>
                        }
                    </MyShoppingCart>
                </LinkCart>

                <ContainerInput id = "input">
                    <Input type="text" placeholder="¿QUE ESTAS BUSCANDO?" />      
                </ContainerInput>
            </ContainerHeader>

            <ContainerTitle>
                <Title>Compras Premium</Title>
                <Span>Crea tu cuenta y haste premium</Span>
                <Button>Crear una cuenta</Button>
            </ContainerTitle>
        </Container>
    )
}

export default Header