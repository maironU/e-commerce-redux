import styled from 'styled-components'
import {Link as LinkRouter} from 'react-router-dom';

export const Container = styled.div`
    width: 100vw;
    height: 450px;
    position: relative;
`
export const Background = styled.img`
    width: 100%;
    height: 100%;
`
export const ContainerHeader = styled.div`
    width: 100vw;
    background-color: rgba(0, 0, 0, 0);
    display: flex;
    align-items: center;    
    position: fixed;
    top: 0;
    z-index: 100;

    @media (max-width: 768px) {
        flex-flow: row wrap;
        background-color: rgba(0, 0, 0, 1);
        height: 70px;
	}
`
export const ContainerLogo = styled.div`
    width: 15%;
    height: 50px;

    @media (max-width: 768px) {
        width: 100%;
        height: 70px;
        display: flex;
        justify-content: space-between;
        align-items: center;
	}
`
export const LinkLogo = styled(LinkRouter)`
    width: 100px;
    height: 40px; 

`
export const Logo = styled.img`
    width: 100%;
    height: 100%; 
`
export const ContainerLogoMenu = styled.div`
    z-index: 20;
    display: flex;
    align-items: center;
    margin-right: 10px;

    @media (min-width: 769px) {
        display: none;
	}
`
export const Ul = styled.ul`
    width: 45%;
    display: flex;
    align-items: center;
    font-size: 11px;

    @media (max-width: 768px) {
        padding: 0;
        flex-direction: column;
        align-items: normal;
        display: none;
        width: 100%;
        background-color: rgba(0,0,0,1);
        margin-top: 0;
        margin-bottom: 0;
	}
`
export const Li = styled.li`
    list-style: none;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    font-size: 15px;
    margin-right: 15px;

    @media (max-width: 768px) {
        justify-content: normal;
        width: 100%;
	}
    
`
export const Link = styled(LinkRouter)`
    text-decoration: none;
    color: white;
    padding: 10px;

    :hover {
        color: #FE2727;
    }

    @media (max-width: 768px) {
        width: 100%;
	}
`
export const LinkCart = styled(LinkRouter)`
    text-decoration: none;
    margin-right: 50px;

    @media (max-width: 768px) {
        position: absolute;
        top: 15.5px;
        left: 50%;
	}
`
export const ContainerInput = styled.div`
    width: 20%;
    display: flex;
    justify-content: center;

    @media (max-width: 768px) {
        width: 100%;
        display: none;
        margin-bottom: 15px;
        background-color: rgba(0,0,0,1);
        justify-content: normal;
        position: absolute;
        top: 180px;
    }
`
export const Input = styled.input`
    width: 100%;
    background: #FFF;
    border: 1px solid #D6D6D6;
    color: #525252;
    display: block;
    margin: 0 auto;
    padding: 10px;
    box-sizing: border-box;
    font-size: 9px;
    font-weight: 400;
    font-style: normal;
    text-transform: uppercase;
    border-radius: 5px;
    text-align: center;

    :focus {
        outline: none;
    }

    @media (max-width: 768px) {
        width: 40%;
        margin: 10px 0 10px 10px;
	}

`
export const ContainerTitle = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    justfy-content: center;
    align-items: center;
    position: absolute;
    top: 35%;
    margin: 0 auto;
`
export const Title = styled.h1`
    color: white;
`
export const Span = styled.span`
    color: #D5D5D5;
    font-size: 12px;
    margin: 15px 0 15px 0;
`
export const Button = styled.button`
    width: 150px;
    border: none;
    padding: 15px;
    background: #DC9D16;
    border-radius: 5px;
    font-weight: 400;
    color: white;
    cursor: pointer;
`
export const MyShoppingCart = styled.div`
    position: relative;
`
export const Cant = styled.span`
    cursor: pointer;
    left: 25px;
    top: -6px;
    position: absolute;
    background: blue;
    border-radius: 40%;
    padding: 0 5px 0 5px;
    font-size: 12px;
    font-weight: 600;
    color: white;
`