import styled, {keyframes} from 'styled-components'
import {Link as LinkRouter} from 'react-router-dom';

export const ContainerModal = styled.div`
    position: fixed;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 100;
    background: rgba(0, 0, 0, 0.5);
    display: flex;
    align-items: center;
    justify-content: center;
`
export const Modal = styled.div`
    width: 40%;
    height: auto;
    background: white;
    z-index: 200; 
    padding: 40px;
    border-radius: 10px;

    @media (max-width: 768px) {
        width: 70%;
    }
`
export const Title = styled.h2`
    text-align: center;
`
export const Link = styled(LinkRouter)`
    text-decoration: none;
    display: block;
    padding: 10px;
    background: #f50;
    color: white;
    text-align: center;
    font-size: 20px;
`
export const Close = styled(LinkRouter)`
    text-align: center;
    display: block;
    font-size: 15px;
    margin-top: 15px;
`