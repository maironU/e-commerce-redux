import React from 'react'
import { ContainerModal, Modal, Title, Link, Close } from './styled'

const ModalAdd = () => {   

    return(
        <>    
            <ContainerModal>
                <Modal>
                    <Title>El Producto se añadió al carrito</Title>
                    <Link to = "/cart">Ir al carrito</Link>
                    <Close to = "/shop">Seguir Comprando</Close>
                </Modal>
            </ContainerModal>
            
        </>
    )
} 

export default ModalAdd