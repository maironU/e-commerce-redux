import { createGlobalStyle } from 'styled-components'

const GlobalStyled = createGlobalStyle`
	body {
		font-family: 'Roboto', sans-serif;
		margin: 0;
		padding: 0;
        box-sizing: border-box;
        overflow-x:hidden;
	}
`
export default GlobalStyled
