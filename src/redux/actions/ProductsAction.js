import Axios from 'axios'

export const LOADING = "LOADING"
export const SUCCESS = "SUCCESS"
export const ERROR = "ERROR"


export const Loading = () => {
    return {
        type: LOADING,
    }
}

export const Success = (products) => {
    return {
        type: SUCCESS,
        payload: products
    }
}

export const Error = (error) => {
    return {
        type: ERROR,
        payload: error
    }
}

const FetchProducts = () => {
    return (dispatch) => {
        dispatch(Loading())
        Axios.get(`http://192.168.1.4:8000/api/moteles/mostrar`)
        .then(response => {
            dispatch(Success([response.data]))
        })
        .catch(error => {
            dispatch(Error("Error"))
        })
    }
}

export default FetchProducts