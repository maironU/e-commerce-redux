import Axios from 'axios'

export const LOADING_PRODUCT_WITH_ID = "LOADING_PRODUCT_WITH_ID"
export const SUCCESS_PRODUCT_WITH_ID = "SUCCESS_PRODUCT_WITH_ID"
export const ERROR_PRODUCT_WITH_ID = "ERROR_PRODUCT_WITH_ID"


export const LoadingProductWithId = () => {
    return {
        type: LOADING_PRODUCT_WITH_ID,
    }
}

export const SuccessProductWithId = (product) => {
    return {
        type: SUCCESS_PRODUCT_WITH_ID,
        payload: product
    }
}

export const ErrorProductWithId = (error) => {
    return {
        type: ERROR_PRODUCT_WITH_ID,
        payload: error
    }
}

const FetchProductWithId = (id) => {
    console.log(id)
    return (dispatch) => {
        dispatch(LoadingProductWithId())
        Axios.get(`http://192.168.1.4:8000/api/moteles/mostrar/${id}`)
        .then(response => {
            dispatch(SuccessProductWithId([response.data]))
        })
        .catch(error => {
            dispatch(ErrorProductWithId("Error"))
        })
    }
}

export default FetchProductWithId