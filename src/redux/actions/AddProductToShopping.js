export const ADD_PRODUCT_TO_SHOPPING = "ADD_PRODUCT_TO_SHOPPING"

const AddProductToShopping = (product, cantidad) => {

    const objeto = [{
        "product": product,
        "cantidad": cantidad
    }]

    const cart = localStorage.getItem("cart")
    const parseado = JSON.parse(cart)

    if(parseado != null){

        for(var i = 0; i < parseado.length; i++){
            if(parseado[i].product.motel_id === product.motel_id){
                parseado[i].cantidad = parseInt(parseado[i].cantidad) + parseInt(cantidad)
                localStorage.setItem("cart", JSON.stringify(parseado))

                break
            }else{
                const newCar = parseado.concat(objeto)
                localStorage.setItem("cart",JSON.stringify(newCar))
            }
        }

    }else{
        localStorage.setItem("cart",JSON.stringify(objeto))
    }
    
    return {
        type: ADD_PRODUCT_TO_SHOPPING,
    }
}

export default AddProductToShopping 