export const DELETE_PRODUCT_TO_SHOPPING = "DELETE_PRODUCT_TO_SHOPPING"

const DeleteProductToShopping = (Myproduct) => {

    const cart = localStorage.getItem("cart")
    const parseado = JSON.parse(cart)
    console.log(parseado)
    console.log(Myproduct.product.motel_id)
    if(parseado != null){

        for(var i = 0; i < parseado.length; i++){
            if(parseado[i].product.motel_id === Myproduct.product.motel_id){
                
                parseado.filter(product => product.product.motel_id !== Myproduct.product.motel_id)
                localStorage.setItem("cart", JSON.stringify(parseado.filter(product => product.product.motel_id !== Myproduct.product.motel_id)))

                if(parseado.filter(product => product.product.motel_id !== Myproduct.product.motel_id).length === 0){
                    localStorage.removeItem("cart")
                }

                break
            }else{
                localStorage.setItem("cart",JSON.stringify(parseado))
            }
        }

    }

    return {
        type: DELETE_PRODUCT_TO_SHOPPING,
    }
}

export default DeleteProductToShopping