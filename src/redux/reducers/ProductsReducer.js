const initialState = {
    loading: false,
    products: [],
    error : ""
}

const ProductsReducer = (state = initialState, action) => {
    switch(action.type){
        case "LOADING":
            return {
                ...state,
                loading: true
            }
        case "SUCCESS":
            return {
                ...state,
                products: action.payload,
                loading: false,
                error: ""
            }
        case "ERROR":
            return {
                error: action.payload,
                loading: false,
                products: []
            }
        default: 
            return state
    }
}

export default ProductsReducer