const initialState = {
    loading: false,
    product: [],
    error : ""
}

const ProductWithIdReducer = (state = initialState, action) => {
    switch(action.type){
        case "LOADING_PRODUCT_WITH_ID":
            return {
                ...state,
                loading: true
            }
        case "SUCCESS_PRODUCT_WITH_ID":
            return {
                ...state,
                product: action.payload,
                loading: false,
                error: ""
            }
        case "ERROR_PRODUCT_WITH_ID":
            return {
                error: action.payload,
                loading: false,
                product: []
            }
        default: 
            return state
    }
}

export default ProductWithIdReducer