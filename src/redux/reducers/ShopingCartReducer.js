import {ADD_PRODUCT_TO_SHOPPING} from '../actions/AddProductToShopping'
import {DELETE_PRODUCT_TO_SHOPPING} from '../actions/DeleteProductToShopping'

const initialState = {
    agregado: false,
    eliminado: false
}

const ShoppingCartReducer = (state = initialState, action) => {
    switch(action.type){    
        case ADD_PRODUCT_TO_SHOPPING:
            return {
                ...state,
                agregado: true,
            }     
        case DELETE_PRODUCT_TO_SHOPPING:
            return {
                ...state,
                eliminado: true
            }
        default:
            return state
    }
}

export default ShoppingCartReducer