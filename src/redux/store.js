import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import ProductsReducer from './reducers/ProductsReducer'
import ShopingCartReducer from './reducers/ShopingCartReducer'
import ProductWithIdReducer from './reducers/ProductWithIdReducer'

const reducers =  combineReducers({
    ProductsReducer,
    ShopingCartReducer,
    ProductWithIdReducer,
})

const store = createStore(
    reducers,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
)

export default store