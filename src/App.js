import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter, Switch, Redirect, Route} from 'react-router-dom'
import Shop from './Pages/Shop'
import ProductDetails from './Pages/ProductDetails'
import MyCart from './Pages/MyCart'
import store from './redux/store'
import GlobalStyled from './Styled/GlobalStyled'

const App = () => {
    return(
        <Provider store = {store}>
            <GlobalStyled />
            <BrowserRouter>
                <Switch>
                    <Route exact path = "/shop" component = {Shop} />
                    <Route exact path = "/product/:product_id" component = {ProductDetails} />
                    <Route exact path = "/cart" component = {MyCart} />
                    <Redirect from = "/" to = "/shop" />
                </Switch>
            </BrowserRouter>
        </Provider>
    )   
}

export default App