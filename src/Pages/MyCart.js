import React from 'react'
import ShoppingCart from '../Components/ShoppingCart'
import Header from '../Components/Header'

const MyCart = () => {
    return(
        <>
            <Header />
            <ShoppingCart />
        </>
    )
}

export default MyCart