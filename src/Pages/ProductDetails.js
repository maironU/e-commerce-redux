import React, { Fragment } from 'react'
import Details from '../Components/Details'
import Header from '../Components/Header'
import Preloader from '../Components/Preloader'

import {
    useParams
  } from "react-router-dom";
  

const ProductDetails = () => {

    let { product_id } = useParams()

    return(
        <Fragment>
            <Preloader />
            <Header />
            <Details product_id = {product_id}/>
        </Fragment>
    )
}

export default ProductDetails