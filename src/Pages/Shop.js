import React, { Fragment } from 'react'
import Header from '../Components/Header'
import Body from '../Components/Body'
import Preloader from '../Components/Preloader'

const Shop = () => {
    return(
        <Fragment>
            <Preloader />
            <Header />
            <Body />
        </Fragment>
    )
}

export default Shop